
# README #
This readme is for writing a good PR.

### current PR template ###
* What does this PR do?

* Why do we want to do that?‌

* What high level changes did you make to the code to accomplish that goal?

* ‌What other information should the reviewer be aware of when looking at this code?

* How was it tested?



### Detailed Example ###
* What does this PR do?
	* a brief description of what this pr is for
	* etc. To create a new api which returns a list of experts
* Why do we want to do that?‌
	* a description on why this feature/change is needed
	* etc. Allows the frontend to get all the experts for a request
* What high level changes did you make to the code to accomplish that goal?
	* a simple list of things you did to make the change
	* etc...
		* Added new expert controller
		* Created new model
		* Added new services

* ‌What other information should the reviewer be aware of when looking at this code?
	* related changes that the reviewer should know about
	* etc.
		* the list of PRs for this change
		* a change of functionality

* How was it tested?
	* how did you test this new feature
	* etc.
		* Tested api through swagger and made sure response was accurate
		* Tested passing in invalid value and the correct error was thrown

### Real PR Examples ###
[PR]([https://bitbucket.org/newtonx-team/nx-backend-api/pull-requests/268](https://bitbucket.org/newtonx-team/nx-backend-api/pull-requests/268))